<?php 
class Home extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_tulisan');
		$this->load->model('m_pengunjung');
        $this->m_pengunjung->count_visitor();
	}
	function index(){
		$x['post']=$this->m_tulisan->get_post_home();
		$x['banners']=$this->m_tulisan->get_all_banners();
		$this->load->view('v_home',$x);
	}
}