<?php
class Banner extends CI_Controller{
	function __construct(){
		parent::__construct();
		if(!isset($_SESSION['logged_in'])){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('m_album');
		$this->load->model('m_galeri');
		$this->load->model('m_banner');
		$this->load->model('m_pengguna');
		$this->load->library('upload');
	}


	function index(){

		$x['data']=$this->m_banner->get_all_banner();
		$this->load->view('admin/v_banner',$x);
	}
	
	function simpan_banner(){
				$config['upload_path'] = './theme/banners/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./theme/banners/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 500;
	                        $config['height']= 400;
	                        $config['new_image']= './theme/banners/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $gambar=$gbr['file_name'];
	                        $name=strip_tags($this->input->post('xname'));
							$kode=$this->session->userdata('idadmin');
							$user=$this->m_pengguna->get_pengguna_login($kode);
							$p=$user->row_array();
							$user_id=$p['pengguna_id'];
							$user_nama=$p['pengguna_nama'];
							$this->m_banner->simpan_banner($name,$gambar);
							echo $this->session->set_flashdata('msg','success');
							redirect('admin/banner');
					}else{
	                    echo $this->session->set_flashdata('msg','warning');
	                    redirect('admin/banner');
	                }
	                 
	            }else{
					redirect('admin/banner');
				}
				
	}
	
	function update_banner(){
				
	            $config['upload_path'] = './theme/banners/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        $config['image_library']='gd2';
	                        $config['source_image']='./theme/banners/'.$gbr['file_name'];
	                        $config['create_thumb']= FALSE;
	                        $config['maintain_ratio']= FALSE;
	                        $config['quality']= '60%';
	                        $config['width']= 500;
	                        $config['height']= 400;
	                        $config['new_image']= './assets/images/'.$gbr['file_name'];
	                        $this->load->library('image_lib', $config);
	                        $this->image_lib->resize();

	                        $gambar=$gbr['file_name'];
	                        $banner_id=$this->input->post('kode');
	                        $name=strip_tags($this->input->post('xname'));
							$images=$this->input->post('gambar');
							$path='./theme/banners/'.$images;
							unlink($path);
							$kode=$this->session->userdata('idadmin');
							$user=$this->m_pengguna->get_pengguna_login($kode);
							$p=$user->row_array();
							$user_id=$p['pengguna_id'];
							$user_nama=$p['pengguna_nama'];
							$this->m_banner->update_banner($banner_id,$name,$gambar);
							echo $this->session->set_flashdata('msg','info');
							redirect('admin/banner');
	                    
	                }else{
	                    echo $this->session->set_flashdata('msg','warning');
	                    redirect('admin/banner');
	                }
	                
	            }else{
							$banner_id=$this->input->post('kode');
	                      	$name=strip_tags($this->input->post('xname'));
							$album=strip_tags($this->input->post('xalbum'));
							$kode=$this->session->userdata('idadmin');
							$user=$this->m_pengguna->get_pengguna_login($kode);
							$p=$user->row_array();
							$user_id=$p['pengguna_id'];
							$user_nama=$p['pengguna_nama'];
							$this->m_banner->update_banner_tanpa_image($banner_id,$name);
							echo $this->session->set_flashdata('msg','info');
							redirect('admin/banner');
	            } 

	}

	function hapus_banner(){
		$kode=$this->input->post('kode');
		$gambar=$this->input->post('gambar');
		$path='./theme/banners/'.$gambar;
		unlink($path);
		$this->m_banner->hapus_banner($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/banner');
	}

}